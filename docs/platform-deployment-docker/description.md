---
id: description
title: Platform architecture description
---

This section describes the components and the related containers used to make the working prototype of the architecture. Following diagram depicts the architecture and interactions among the containers.

 ![image alt text](assets/platform-deployment-docker/docker-synch-arch.png)

This repository provides a `docker-compose.yml`, where all the architecture components are defined as containers (namely services).
If you want to use only a subset of them, comment with "#" the corresponding rows of the related container.

## Docker Networking  
The Docker Compose will create a bridge network (see [here](https://docs.docker.com/compose/networking/)), named **`main_synchronicity`**. The containers will be attached to this network and each one will have its own assigned IP and hostname, internal to the network. In particular, the container hostname will be equal to the name given in the “services” section of docker-compose file. Thus, each container can look up the hostname of the others; this will be important when configuring components to communicate to each other.

This can be checked by running:

* **`docker network ls`**, to see all the docker network, including the created one (**main_synhcronicity**), where all the containers will be attached to.

* **`docker inspect network main_synchronicity`**, to check IPs assigned to the running containers.

**NOTE**
As the network is a bridge, each port exposed by containers (e.g. 1026), will be mapped and also reachable in the machine where Docker was installed. Thus, if the machine is publicly and directly exposed, also that ports will be reachable, unless they were closed.
***
## Components description
This section describes in detail each component included in the architecture.
As depicted in the figure, each component will have its persistence layer running as a separate Mongo DB Container. In particular, each one will have a mounted volume, that is a host folder, where the DB data will be stored.

### Orion Context Broker

The Context Broker is in charge of storing entities, which represent the mapped southbound devices and relative attributes (active, lazy, commands). It exposes NGSI APIs , in order to manage the lifecycle of an entity and related subscriptions.

It does not hold an historical view of entity values, this is performed by the Cygnus Connector and STH-Comet components.

**Public endpoint**: [https://services.synchronicity-iot.eu/api/context](https://services.synchronicity-iot.eu/api/context)

**API Reference**: [https://github.com/telefonicaid/fiware-orion#api-walkthrough ](https://github.com/telefonicaid/fiware-orion)

### Cygnus Connector

The Cygnus component has the role of connector between the Context Broker and several storage systems. In particular, it enables to create and store an historical view of the entities and related attributes issued by the Context Broker.

It supports several «sinks», plugins to write in different persistence systems:

* MongoDB
* MySQL
* CKAN
* PostgresSQL
* STH

Cygnus uses the subscription/notification feature of the Context Broker. A subscription is made in Orion on behalf of Cygnus, detailing which entities we want to be notified to Cygnus when an update occurs on any of those entities attributes.

In particular Cygnus listens for notifications on the **(internal) endpoint**: "[http://cygnus:5050/notify](http://cygnus:5050/notify)"

**Reference**:[ https://github.com/telefonicaid/fiware-cygnus](https://github.com/telefonicaid/fiware-cygnus)

### STH-Comet for SynchroniCity

The Short Time Historic (STH, aka. Comet) is the component in charge of managing (storing and retrieving) historical raw and aggregated time series information about the evolution in time of context data (i.e., entity attribute values) registered in the Context Broker instance. This is an extended version of FIWARE Short Time Historic (STH) - Comet, a component able to manage (storing and retrieving) historical context information as raw and aggregated time series context information. This version adds the   SynchroniCity Historical data retrieval API, to those already existing and provided by STH-Comet.

In this architecture, the **_formal_** way is used (see [here](https://fiware-sth-comet.readthedocs.io/en/latest/data-storage/index.html)). It uses the Cygnus component to register the raw and aggregated time series context information into a mongo container, then used by the STH component. The subscription of the entities then is still performed between Cygnus and Orion Context Broker.

Cygnus is configured by default (with the provided `agent.conf` file, see Configuration section), with the following sinks:

* **Mongo-sink**: used for storing historical raw context information.
* **Sth-sink**: used for storing historical aggregated time series context information.

Both sinks store information in the MongoDB container (**mongo-cygnus**).

STH exposes an HTTP REST API to let external clients query the available historical context information:

*  **Raw (see [here ](https://fiware-sth-comet.readthedocs.io/en/latest/raw-data-retrieval/index.html))**
*  **Aggregated time series (see [here](https://fiware-sth-comet.readthedocs.io/en/latest/aggregated-data-retrieval/index.html))**
*  **SynchroniCity Historical data retrieval API**: The STH component, as Synchronicity customization, allows to get historical data related to an attribute of an entity, according to the API definition provided [here](https://synchronicityiot.docs.apiary.io/#reference/data-storage-api-historical/historical-data-retrieval).

**Public endpoint**: [ https://services.synchronicity-iot.eu/api/historical](https://services.synchronicity-iot.eu/api/historical)

**Original Fiware STH-Comet Reference**: [https://fiware-sth-comet.readthedocs.io/en/latest/index.html](https://fiware-sth-comet.readthedocs.io/en/latest/index.html)
**SynchroniCity custom version**:
[https://gitlab.com/synchronicity-iot/sth-comet-synchronicity/blob/master/README.md](https://gitlab.com/synchronicity-iot/sth-comet-synchronicity/blob/master/README.md)
### IoT Agent Manager (IDAS)

The IoT Agent Manager works as a proxy for scenarios where multiple IoT Agents offer different southbound protocols. It is a single administration endpoint for agent provisioning tasks, redirecting NGSI requests to the appropriate IoTAgent based on the declared protocol.

Each registered Agent is uniquely identified by the following two parameters :

* **Protocol**: Name of the protocol served by the IoTAgent
* **Resource-APIKey**: Unique pair of strings used to identify different IoT Agents for the same protocol.

It exposes the Subscription APIs to manage agent provisioning and the services to be redirected to the appropriate Agent.

**Public endpoint**:[ https://iot-agent.synchcity.eu]( https://iot-agent.synchcity.eu)

**Reference**:[ https://github.com/telefonicaid/iotagent-manager](https://github.com/telefonicaid/iotagent-manager)

### IoT Agent UltraLight

An IoT Agent is a component that lets a groups of devices send their data to and be managed from a FIWARE NGSI Context Broker using their own native protocols, such as UltraLight protocol for the measure payload. It manages the device registration, by specifying its attributes and related mapping to a NGSI context entity.
* Each **Device** will be mapped as an **Entity** associated to a Context Provider.
* Each **Measure** obtained from the device will be mapped to a different **entity attribute**.

Device measures can have three different behaviors:

* **Active** –  pushed from the device to the IoT agent.
* **Lazy** – pass device will wait for the IoT Agent to request for data.
* **Commands** – Special attribute if the device can accept commands.

The provision process is meant to provide the IoTA with the following information:

* Entity translation information: information about how to convert the data coming from the South Bound device into NGSI information. E.g. entity name, type and attributes that will be created in the entity.
* Southbound protocol identification: To identify a particular device when a new measure comes to the Southbound (**Device ID** and **API Key**).

The UltraLight payload can be carried out through two South Bound transport protocols:

* **HTTP** : The agent itself exposes HTTP southbound APIs, in order to accept values coming from Devices.

    *  It is available at the **public endpoint**: [https://ul-http.synchcity.eu](https://ul-http.synchcity.eu)

* **MQTT**: The agent subscribes to a device-specific topic, by using the MQTT broker Mosquitto.

    * An instance of Mosquitto is provided with the docker-compose file, it listens to the 1883 port, which is internal to the host machine and reachable by the iot-agents.

For both protocols, the IoT Agent container has a console client, for simulating the device measrements.

**Public endpoint**: [https://ul-admin.synchcity.eu/](https://ul-admin.synchcity.eu/)

**Reference**:[ https://github.com/telefonicaid/iotagent-ul](https://github.com/telefonicaid/iotagent-ul)


## Security Layer

The security layer is in charge of providing Authentication and Authorization to the whole architecture prototype. In particular, the main focus is to grant access to the backend APIs, exposed by the described components, only to registered and authenticated users, by implementing the OAuth2 flow.

**Authentication**

In detail, the Authentication part is composed by the IdM and Pep Proxy components.

### IdM - Keyrock

IdM offers tools for administrators to support the handling of user life-cycle functions. It reduces the effort for account creation and management, as it supports the enforcement of policies and procedures for user registration, user profile management and the modification of user accounts. It allows to link an application with the user account, in order to enable it to authenticate the users, by the interaction with the IdM Oauth APIs (see [here](http://fiware-idm.readthedocs.io/en/latest/oauth2.html)). In detail, the user will register the PEP-proxy to an application linked with its own account (see [here](http://fiware-idm.readthedocs.io/en/latest/user_guide.html) and also [here ](https://edu.fiware.org/course/view.php?id=79)for video tutorials).

The IdM is composed of two separated services, that interact with each other. The web portal is based on OpenStack’s Dashboard, **Horizon**. The back-end is a REST service based on OpenStack’s Identity Provider, **Keystone**.

Horizon internal endpoint: [http://localhost:8000](http://localhost:8000)

Keystone internal endpoint: http://localhost:8001

**Portal public endpoint:** [https://services.synchronicity-iot.eu/api/security/](https://services.synchronicity-iot.eu/api/security/)

**Reference**: [http://fiware-idm.readthedocs.io/en/latest/ ](http://fiware-idm.readthedocs.io/en/latest/)

### Pep Proxy - Wilma

The PEP Proxy GE is a backend component, without frontend interface. It is in charge of intercepting all the requests to the backend APIs and to check if the token included in the requests correspond to an authenticated user, by performing a token validation against the IdM. If the token validation succeeds, the proxy forwards finally the request to the requested API.

In order to use the secured APIs, the user must register an account in the IdM ([https://auth.synchcity.eu](https://auth.synchcity.eu)) and register a new application. It will be an application that requires to access some secured API of the platform. Then the user must register the Pep-Proxy in the application (as described in the video [here](https://edu.fiware.org/mod/url/view.php?id=735)).

All the requests must have the **X-Auth-Token** header, with the token provided by the IdM Oauth2 services. When a user logs in, IdM will generate an OAuth2 token that represents it.

This is the "basic use case" of the Pep-Proxy User Guide ([here](http://fiware-pep-proxy.readthedocs.io/en/latest/user_guide/)), in particular the Level 1 Authentication, as depicted below. The “Backend apps” are the secured APIs exposed by the architecture components.

![image alt text](assets/platform-deployment-docker/image_1.png)

**Reference**: [http://fiware-pep-proxy.readthedocs.io/en/latest/](http://fiware-pep-proxy.readthedocs.io/en/latest/)

### PEP Proxy - Wilma Plus
This component is an extended version of the PEP Proxy - Wilma FIWARE GE, described in the previous version. This version enables to:
- use different versions of the Keyrock IdM.
- use one PEP Proxy instance to secure both backend application (e.g. Orion) and the Historical API endpoin
- manage specific permissions and policies to your resources allowing different access levels to your users.

**Reference**
For further information about its installation and configuration, see the relative documentation in the Project repository [(here)](https://gitlab.com/synchronicity-iot/fiware-pep-proxy).
