---
id: endpoints
title: Summary table of public instance endpoints
---

This table reports the public endpoints where the reference instance of the SynchroniCity project is available.
|Public Endpoint|Description|
|----------------------------------------------------------------------------------|----------------------------------------------------------------------|
|https://services.synchronicity-iot.eu/api/context|Orion Context Broker endpoint|
|https://cygnus.synchcity.eu|Cygnus administration endpoint|
|https://iot-agent.synchcity.eu|IoT Agent Manager|
|https://ul-http.synchcity.eu|http endpoint accepting Ultra Light payload from known devices|
|https://ul-admin.synchcity.eu|UltraLight Agent administration endpoint|
|https://services.synchronicity-iot.eu/api/historical|STH-Comet-SynchroniCity Historical API endpoint|
|https://services.synchronicity-iot.eu|IdM Portal Endpoint|
