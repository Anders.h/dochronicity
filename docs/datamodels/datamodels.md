---
id: datamodels
title: Understanding Datamodels
sidebar_label: Understanding Datamodels
---
# SynchroniCity Data Models

## What is a Data Model?
The Synchronicity framework is based in the management of Context Entities, intended as Json representations stored in the [Context Broker](https://fiware-orion.readthedocs.io/) and accessed through the [Context Management API](https://synchronicityiot.docs.apiary.io/#reference/context-management-api). Those entities, in theory, could have any attribute with any data type; the flexiblity of having entities representing etherogeneous data could break the harmonization and the interoperability, which are critical concerns within the SynchroniCity project. Altought these flexibility was preserved with the possiblity of having additional/optional fields in the entities, a set of Data Models have been adopted. These Data Models, which are formally defined through Json Schemas, determine which mandatory and optional fields an entity compliant to a specific Data Model must have. In addition, they define for each field, their data types (e.g. string, number or object) and often also the accepted values (e.g. enums): for instance, the `category` field of a Point Of Interest. The Data Models cover a wide range of themes in the smart cities area, divided into several verticals:
- **Environment**
- **PointOfInterest**
- **Transportation**
- **Weather**
- **Urban Mobility**
- **Events**
- **Parking**
- **Parks & Gardens**
- **Waste Management**

## How does a Data Model look like ?
The JSON Schema definition of a Data Model, which defines mandatory and optional fields, have the following form:
```
{
	"$schema": "http://json-schema.org/schema#",
	"id": "https://fiware.github.io/dataModels/specs/Transportation/BikeHire/schema.json",
	"title": "FIWARE - Transportation / Bike Hire Docking Station",
	"description": "Bike Hire Docking Station",
	"type": "object",
	"allOf": [
		{
		  "$ref": "https://fiware.github.io/dataModels/common-schema.json#/definitions/GSMA-Commons"
		},
		{
		  "$ref": "https://fiware.github.io/dataModels/common-schema.json#/definitions/Location-Commons"
		},
		{
			"properties": {
				"type": {
					"type": "string",
					"enum": [
						"BikeHireDockingStation"
					],
					"description": "NGSI Entity type"
				},
				"totalSlotNumber": {
					"type": "integer",
					"minimum": 1
				},
				"freeSlotNumber": {
					"type": "integer",
					"minimum": 0
				},
				"availableBikeNumber": {
					"type": "integer",
					"minimum": 0
				},
				"outOfServiceSlotNumber": {
					"type": "integer",
					"minimum": 0
				},
				"openingHours": {
					"type": "string"
				},
				"status": {
					"type": "string",
					"enum": [
						"working",
						"outOfService",
						"withIncidence",
						"full",
						"almostFull",
						"empty",
						"almostEmpty"
					]
				},
				"owner": {
					"type": "string"
				},
				"provider": {
					"type": "object"
				},
				"contactPoint": {
					"type": "object"
				}
			}
	}],
	"required": [
		"id",
		"type"
	]
}
```

Every entity compliant to a Data Model must have a set of common fields, as referred in the `allOf` part (`GSMA-Commons` and `Location-Commons`). In the last part the required array defines the required fields for that model: every entity have `id` and `type` fields at least.

An example of **entity compliant** to the `BikeHireDockingStation` reported above, can be the following:
```
{
    "id": "urn:ngsi-ld:BikeHireDockingStation:Milan:BikeSharing:GeoJson:001Duomo1",
    "type": "BikeHireDockingStation",
    "address": {
        "streetAddress": "P.za Duomo"
    },
    "location": {
        "type": "Point",
        "coordinates": [
            9.189042572,
            45.464725436
        ]
    },
    "name": "001 Duomo 1",
    "totalSlotNumber": 24
}
```

## How to use it
A Data Model must be used when storing/using Context Entities from/to a Context Broker.
That entity must pass validation against the Json Schema of the specific Data Model. 
### Validation
- There are different online JSON Schema Validators, for instance: http://jsonschemalint.com/. 
- For the development of these schemas the [AJV JSON Schema Validator](https://github.com/epoberezkin/ajv) is being used, which has also a [CLI tool](https://www.npmjs.com/package/ajv-cli) to be installed with `npm` and a `validate.sh` can be found [here](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/validate.sh) .
- Finally, SynchroniCity developed a **tool to validate entities against the data models**. Code and documentation can be found [here](https://gitlab.com/synchronicity-iot/rz-instance-validator).

### Mapping
If you have either CSV, Json, or GeoJson data, that could be translated to a SynchroniCity Data Model by a mapping process, you can use the provided [Data Model Mapper](https://docs.synchronicity-iot.eu/docs/datamodels/datamodelmapper).

## List of SynchroniCity Data Models
The full list of SynchroniCity Data Model for each vertical, including descriptions and JSON Schema definitions, can be found in the [SynchroniCity repository](https://gitlab.com/synchronicity-iot/synchronicity-data-models)

## How to contribute

Contributions should come in the form of merge requests done in the [SynchroniCity repository](https://gitlab.com/synchronicity-iot/synchronicity-data-models)

### Data models guidelines

SynchroniCity provides a set of [guidelines](./guidelines.md) for defining new data models. Further information can be found in [SynchroniCity Deliverable D2.2](https://synchronicity-iot.eu/wp-content/uploads/2018/05/synchronicity_d2_2_guidelines_for_the_definition_of_oasc_shared_data_models.pdf)

