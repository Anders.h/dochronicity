---
id: deployment
title: Deploying the IoT Data Marketplace
sidebar_label: Deployment
---
The IoT data marketplace can be deployed using Docker. A Docker image is provided for all the components of the SynchroniCity IoT data marketplace which is based on the FIWARE / TM Forum Business API Ecosystem.
The IoT data marketplace requires instances of MySQL and MongoDB to be running. If you are deploying using `docker-compose`, as explained next, this will be done automatically. However, you can also use your own direct or dockerized deployments of MySQL and MongoDB and adjust the configuration accordingly.

## Docker image

First of all, clone the repository: https://gitlab.com/synchronicity-iot/synchronicitydatamarketplace.git

Change the following parts in the `docker/proxy-conf/config.js` file according to your configuration (e.g., replace the placeholders "<>" with the corresponding information).

```
config.proxy = {
	enabled: true,
	host: '<my-host>',
	secured: true,
	port: 443
}
```
```
// OAuth2 configuration
config.oauth2 = {
	'server': '<my-IdM-address>',
	'clientID': '<my-client-id>',
	'clientSecret': '<my-client-secret>',
	'callbackURL': '<my-host>/auth/fiware/callback',
	'roles': {
		'admin': 'provider',
		'customer': 'customer',
		'seller': 'seller',
		'orgAdmin': 'orgAdmin'
	}
};
```
Change the following parts in the `docker/charging-settings/services_settings.py` file according to your local configuration.
```
SITE = '<my-host>'
```
```
AUTHORIZE_SERVICE = 'https://<my-host>/authorizeService/token'
```
```
# Keyrock/Keystone settings

KEYSTONE_PROTOCOL = 'https'
KEYSTONE_HOST = '<my-keystone-host>'

# Use Keyrock7 port for both e.g., 3000

KEYROCK_PORT = '3000'
KEYSTONE_PORT = '3000'

KEYSTONE_USER = '<my-keystone-user>'
KEYSTONE_PWD = '<my-keystone-pwd>'
ADMIN_DOMAIN = 'Default'

# APP SETTINGS (e.g., Orion context broker)
APP_CLIENT_ID = '<my-app-client-id>'

# PEP Proxy Wilma endpoint + /v2/entities
APP_URL = '<my-host>/v2/entities/'
```
More specifically:
-   `KEYSTONE_PROTOCOL`:  this tutorial assumes you use 'https')
-   `KEYSTONE_HOST`: host name where the IdM instance is running (e.g., ‘idm.docker’)
-   `KEYROCK_PORT`: port where the IdM instance is listening
-   `KEYSTONE_PORT`: port where the  IdM instance is listening
-   `KEYSTONE_USER`: admin username on the IdM
-   `KEYSTONE_PWD`: admin password on the IdM
-   `ADMIN_DOMAIN`: admin domain on the IdM (e.g., ‘Default’)
-   `APP_CLIENT_ID`: Client ID of the Orion Context Broker registered on the IdM
-   `APP_CLIENT_SECRET`: Client Secret of the Orion Context Broker registered on the IdM

## Component images and deployment

As stated, to deploy the SynchroniCity IoT data marketplace, we use the Docker images available for each of its
component using `docker-compose`. Specifically, the following images will be deployed:

* [bae-apis-synchronicity](https://hub.docker.com/r/digicatsynch/bae-apis-synchronicity): This image includes our adapted version of the TMForum APIs
* [biz-ecosystem-rss](https://hub.docker.com/r/conwetlab/biz-ecosystem-rss): This image includes the Revenue Sharing component
* [charging-backend-synchronicity](https://hub.docker.com/r/digicatsynch/charging7): This image includes our adapted version of the BAE Charging Backend component
* [logic-proxy-synchronicity](https://hub.docker.com/r/digicatsynch/logic-proxy-synchronicity): This image includes our adapted version of the BAE Logic Proxy component

The easiest way to deploy the SynchroniCity IoT data marketplace with Docker is using a `docker-compose.yml` file which deploys the whole system and databases. 
A running version of this file can be found on this GitLab repo. Change the `PAYPAL_CLIENT_ID` and `PAYPAL_CLIENT_SECRET` variables according to your local configuration.

```
environment:
	- PAYPAL_CLIENT_ID= my-paypal-client-id
	- PAYPAL_CLIENT_SECRET= my-paypal-client-secret
```
Eventually, deploy the IoT data marketplace:

`docker-compose up -d`

Then, the SynchroniCity IoT data marketplace should be up and running on `https://<my-host>/`.

## Check running processes and network interfaces
We need to check that all the processes we need are running. Specifically, Java for the Glassfish server (APIs and RSS), Python (Charging Backend) and Node (Proxy), MongoDB and MySQL.
Open a terminal on your host machine and run:

`ps -ewF | grep 'java\|mongodb\|mysql\|python\|node' | grep -v grep`

You should get something similar to this:

```
mongodb   1014     1  0 3458593 49996 0 sep08 ?        00:22:30 /usr/bin/mongod --config /etc/mongodb.conf
mysql     1055     1  0 598728 64884  2 sep08 ?        00:02:21 /usr/sbin/mysqld
francis+ 15932 27745  0 65187 39668   0 14:53 pts/24   00:00:08 python ./manage.py runserver 0.0.0.0:8006
francis+ 15939 15932  1 83472 38968   0 14:53 pts/24   00:00:21 /home/user/business-ecosystem-charging-backend/src/virtenv/bin/python ./manage.py runserver 0.0.0.0:8006
francis+ 16036 15949  0 330473 163556 0 14:54 pts/25   00:00:08 node server.js
root      1572     1  0 1142607 1314076 3 sep08 ?      00:37:40 /usr/lib/jvm/java-8-oracle/bin/java -cp /opt/biz-ecosystem/glassfish ...
```
Then, to check the listening ports, run:

`$ sudo netstat -nltp`

You should get something similar to this:

```
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 127.0.0.1:8006          0.0.0.0:*               LISTEN      15939/python
tcp        0      0 127.0.0.1:27017         0.0.0.0:*               LISTEN      1014/mongod
tcp        0      0 127.0.0.1:28017         0.0.0.0:*               LISTEN      1014/mongod
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      1055/mysqld
tcp6       0      0 :::80                   :::*                    LISTEN      16036/node
tcp6       0      0 :::8686                 :::*                    LISTEN      1572/java
tcp6       0      0 :::4848                 :::*                    LISTEN      1572/java
tcp6       0      0 :::8080                 :::*                    LISTEN      1572/java
tcp6       0      0 :::8181                 :::*                    LISTEN      1572/java
```

## Databases
Eventually, we have to check that MySQL and MongoDB databases are up and accepting queries. 
As for MySQL, run:

`$ mysql -u <user> -p <password>`

You should get something similar to this:
```
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 174
Server version: 5.5.47-0ubuntu0.14.04.1 (Ubuntu)

Copyright (c) 2000, 2015, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

As for MongoDB, run:

`$ mongo <database> -u <user> -p <password>`

You should get something similar to this:

```
MongoDB shell version: 2.4.9
connecting to: <database>
>
```

To stop the running containers, run:

`docker-compose stop`

To start them again, run:

`docker-compose start`

To remove the containers, run:

`docker-compose down`

To deploy the containers again:

`docker-compose up -d`

Congratulations.

To start using the IoT data marketplace, follow the user guide [here](https://synchronicity-iot-data-marketplace.readthedocs.io/en/latest/user-guide.html).
