---
id: authentication
title: Security and the IoT Data Marketplace
sidebar_label: Authentication
---
The IoT data marketplace plays an important role in realizing the SynchroniCity vision of a Digital Single Market. It allows IoT businesses and cities to exchange and even trade data in order to generate secondary revenue streams out of that. The guarantee that the data available on the marketplace adhere to standardized data formats makes it easy for application developers to replicate their services across different cities/domains.

## OAuth2 authentication

The SynchroniCity IoT data marketplace authenticates the users through the [FIWARE Identity Management (IdM) Generic Enabler](http://fiware-idm.readthedocs.io/en/latest/).
This tutorial assume you are using the FIWARE IdM - Keyrock 7.x. To run the IdM with docker compose, follow the instructions [here](https://github.com/ging/fiware-idm/tree/master/extras/docker#run-the-service-with-docker-compose) (please note that you do not need to build your own image).
 
You can test the IdM using the default user:
- Email: admin@test.com
- Password: 1234

You can change the default credentials in the `Settings` section of the IdM portal.

Please note this tutorial assumes that you will use `https` to run your IoT data marketplace instance. To do that, you have to register your domain, configure a reverse proxy (e.g., NGINX), and obtain a TLS certificate (e.g., by using `certbot`).

To acquire the OAuth2 credentials, you need to register the IoT data marketplace application in the IdM portal. To do that, you have to use the following information. 

* Name: the name of your instance
* URL: `https://<my-host>/`
* Callback URL: to be called in the OAuth2 process. `https://<my-host>/auth/fiware/callback`

In the IdM portal, click on the newly registered application, click on `manage roles` tab and add a new role called `seller`. 

Assuming that you are using the Orion Context Broker for data provisioning, you need to register that in the IdM portal too.
In the IdM portal, click on the newly registered application associated with the Orion Context Broker, click on `manage roles` tab and add a new role called `data_provider`. 

When a user will register in the IoT data marketplace as a `data provider` then the request will be forwarded to the admin of the IdM who will assign these two roles to the user.